var secondState = {
    create: function(){
        this.sea = game.add.tileSprite(0, 0, 800, 600, 'green');
        this.create_system_function();
        this.create_player_function();
        this.create_player2_function();
        this.create_enemy_function();
        this.create_enemy_bullet_function();
        this.create_bullet_function();
        this.create_big_bullet_function();
        this.create_bullet2_function();
        this.create_big_bullet2_function();
        this.create_boss_bullet_function();
        this.create_shot_function();
    },
    update: function(){
        //pause 
        if(game.input.keyboard.isDown(Phaser.Keyboard.P)){
            game.physics.arcade.isPaused=true;
            this.bg_music.pause();
        }
        else 
        if(game.input.keyboard.isDown(Phaser.Keyboard.Q)){
            game.physics.arcade.isPaused=false;
            this.bg_music.resume();
        }

        if(game.physics.arcade.isPaused==false){
        this.sea.tilePosition.y += 2;
        //two things overlap
        game.physics.arcade.overlap(this.bulletPool, this.enemyPool,
            this.enemyHit, null, this);     
        game.physics.arcade.overlap(this.player, this.enemyPool,
            this.playerHit, null, this);      
        game.physics.arcade.overlap(this.bulletPool, this.boss,
            this.bossHit, null, this);     
        game.physics.arcade.overlap(this.player, this.boss,
            this.playerHit, null, this);
        game.physics.arcade.overlap(this.player, this.enemyBullets,
            this.playerHit, null, this);       
        game.physics.arcade.overlap(this.bigbulletPool, this.enemyPool,
            this.enemyHit, null, this);
        game.physics.arcade.overlap(this.bigbulletPoolL, this.enemyPool,
            this.enemyHit, null, this);
        game.physics.arcade.overlap(this.bigbulletPoolR, this.enemyPool,
            this.enemyHit, null, this);    
        game.physics.arcade.overlap(this.bigbulletPool, this.boss,
            this.bossHit, null, this);
        game.physics.arcade.overlap(this.bigbulletPoolL, this.boss,
            this.bossHit, null, this);
        game.physics.arcade.overlap(this.bigbulletPoolR, this.boss,
            this.bossHit, null, this);       
        game.physics.arcade.overlap(this.player, this.bossBullets,
            this.playerHit, null, this);  
        game.physics.arcade.overlap(this.player, this.bossBulletsl,
            this.playerHit, null, this);  
        game.physics.arcade.overlap(this.player, this.bossBulletsr,
            this.playerHit, null, this);       
    //player2       
        game.physics.arcade.overlap(this.bulletPool2, this.enemyPool,
            this.enemyHit, null, this);
        game.physics.arcade.overlap(this.player2, this.enemyPool,
            this.playerHit2, null, this);
        game.physics.arcade.overlap(this.bulletPool2, this.boss,
            this.bossHit, null, this);
        game.physics.arcade.overlap(this.player2, this.boss,
            this.playerHit2, null, this);
        game.physics.arcade.overlap(this.player2, this.enemyBullets,
            this.playerHit2, null, this);
        game.physics.arcade.overlap(this.bigbulletPool2, this.enemyPool,
            this.enemyHit, null, this);
        game.physics.arcade.overlap(this.bigbulletPoolL2, this.enemyPool,
            this.enemyHit, null, this);
        game.physics.arcade.overlap(this.bigbulletPoolR2, this.enemyPool,
            this.enemyHit, null, this);    
        game.physics.arcade.overlap(this.bigbulletPool2, this.boss,
            this.bossHit, null, this);
        game.physics.arcade.overlap(this.bigbulletPoolL2, this.boss,
            this.bossHit, null, this);
        game.physics.arcade.overlap(this.bigbulletPoolR2, this.boss,
            this.bossHit, null, this);  
        game.physics.arcade.overlap(this.player2, this.bossBullets,
            this.playerHit2, null, this);  
        game.physics.arcade.overlap(this.player2, this.bossBulletsl,
            this.playerHit2, null, this);  
        game.physics.arcade.overlap(this.player2, this.bossBulletsr,
            this.playerHit2, null, this);    
        //enemies show up
        if (this.nextEnemyAt<game.time.now && this.enemyPool.countDead()>0) {
            this.nextEnemyAt = game.time.now + this.enemyDelay;
            var enemy = this.enemyPool.getFirstExists(false);            
            enemy.reset(game.rnd.integerInRange(20, 780), 0);
            enemy.body.velocity.y = game.rnd.integerInRange(30, 60);
            enemy.play('fly');
        }
//boss show up
        if(this.score==1500){
            this.create_boss_function();
        }
        if(this.score>1500){
             this.boss_fire();
             this.boss_firel();
             this.boss_firer();
         }
        this.enemy_fire();

        //how to control player
        this.player.body.velocity.x = 0;
        this.player.body.velocity.y = 0;


        if (this.cursors.left.isDown) {
            this.player.body.velocity.x = -this.player.speed;
            if(this.score>1500)this.boss.body.velocity.y = game.rnd.integerInRange(-100, 100);

        }
        else if (this.cursors.right.isDown) {
            this.player.body.velocity.x = this.player.speed;
            if(this.score>1500)this.boss.body.velocity.y = game.rnd.integerInRange(-100, 100);

        }
        
        if (this.cursors.up.isDown) {
            this.player.body.velocity.y = -this.player.speed;
            if(this.score>1500)this.boss.body.velocity.x = game.rnd.integerInRange(-100, 100);
        }
        else if (this.cursors.down.isDown) {
            this.player.body.velocity.y = this.player.speed;
            if(this.score>1500)this.boss.body.velocity.x = game.rnd.integerInRange(-100, 100);

        }
         //player2
         this.player2.body.velocity.x = 0;
         this.player2.body.velocity.y = 0;
         if (game.input.keyboard.isDown(Phaser.Keyboard.A)) {
            this.player2.body.velocity.x = -this.player.speed;
        }
        else if (game.input.keyboard.isDown(Phaser.Keyboard.D)) {
            this.player2.body.velocity.x = this.player.speed;
        }
        
        if (game.input.keyboard.isDown(Phaser.Keyboard.W)) {
            this.player2.body.velocity.y = -this.player.speed;
        }
        else if (game.input.keyboard.isDown(Phaser.Keyboard.S)) {
            this.player2.body.velocity.y = this.player.speed;
        }

        //fire
        if (game.input.keyboard.isDown(Phaser.Keyboard.L) ) {
            this.fire();
        }
        if (game.input.keyboard.isDown(Phaser.Keyboard.K)) {
            this.bigfire();
        }
        if (game.input.keyboard.isDown(Phaser.Keyboard.Z) ) {
            this.fire2();
        }
        if (game.input.keyboard.isDown(Phaser.Keyboard.X)) {
            this.bigfire2();
        }

        if (this.instructions.exists && game.time.now>this.instExpire) {
            this.instructions.destroy();
        }  
    //level up
        if(this.score %1300 == 0 && this.score!=0){
           this.levelup();
        }


    }


    },
    create_system_function(){
        //score
        this.score = 1000;
        this.scoreString = 'Score : ';
        this.scoreText = game.add.text(10, 10, this.scoreString + this.score, { font: '34px Arial', fill: '#fff' });

        this.level = 1;
        this.bg_music = game.add.audio('bg_music');
        this.player_fire = game.add.audio('player_fire_sound');
        this.player_big_fire = game.add.audio('player_big_fire_sound');
        this.bg_music.play();
    },
    create_player_function: function(){
        this.player = game.add.sprite(400, 550, 'player');
        this.player.anchor.setTo(0.5);
        this.player.animations.add('fly', [0, 1, 2], 20, true);
        this.player.play('fly');
        game.physics.arcade.enable(this.player);
        this.player.speed = 300;
        this.player.body.collideWorldBounds = true;
        this.player.body.setSize(20, 20, 0, -5);
        //blood
        this.blood = 3;
        this.bloodString = 'Blood : ';
        this.bloodText = game.add.text(600,10,this.bloodString + this.blood, {font: "35px ", fill: "#ff0044", align: "center" });
    },
    create_player2_function: function(){
        this.player2 = game.add.sprite(400,400,'player2');
        this.player2.anchor.setTo(0.5);
        this.player2.enableBody = true;
        game.physics.arcade.enable(this.player2);
            // this.player2.speed = 300;
            this.player2.body.collideWorldBounds = true;
            //blood
            this.blood2 = 3;
            this.bloodString2 = 'Blood : ';
            this.bloodText2 = game.add.text(600,50,this.bloodString2 + this.blood2, {font: "35px ", fill: "#ff0044", align: "center" });
    },
    create_enemy_function: function(){
        this.enemyPool = game.add.group();
        this.enemyPool.enableBody = true;
        this.enemyPool.createMultiple(50, 'enemy2');
        this.enemyPool.setAll('anchor.x', 0.5);
        this.enemyPool.setAll('anchor.y', 0.5);
        this.enemyPool.setAll('outOfBoundsKill', true);
        this.enemyPool.setAll('checkWorldBounds', true);
        this.enemyPool.forEach(function(enemy) {
        enemy.animations.add('fly',[0, 1, 2], 20, true);
        });
        this.nextEnemyAt = 0;
        this.enemyDelay = 1000;       

    },
    create_boss_function: function(){
        this.boss = game.add.sprite(300, 50, 'boss');
        this.boss.anchor.setTo(0.5);
        this.boss.animations.add('booo',[0, 1, 2], 20, true);
        this.boss.play('booo');
        game.physics.arcade.enable(this.boss);
        this.boss.enableBody = true;
        this.boss.speed = 300;
        this.boss.body.collideWorldBounds = true;
        this.boss.body.setSize(40, 40, 0, -5);
        this.score+=100;//let it return 

        this.boss.body.velocity.x = game.rnd.integerInRange(30, 60);
        this.bossblood = 10;
        this.boss.bulletTime = 0;
        this.boss.bulletTimel = 0;
        this.boss.bulletTimer = 0;

        return;
    },
    create_enemy_bullet_function: function(){
        this.enemyBullets = game.add.group();
        this.enemyBullets.enableBody = true;
        this.enemyBullets.createMultiple(1000, 'enemy_bullet');
        this.enemyBullets.setAll('anchor.x', 0.5);
        this.enemyBullets.setAll('anchor.y', 0.5);
        this.enemyBullets.bulletTime = 0;

    },
    create_bullet_function(){
        this.bulletPool = game.add.group();
        this.bulletPool.enableBody = true;
        this.bulletPool.createMultiple(100, 'bullet');
        this.bulletPool.setAll('anchor.x', 0.5);
        this.bulletPool.setAll('anchor.y', 0.5);  
        this.nextShotAt = 0;
        this.shotDelay = 100;   

        this.bulletTime = 0;
    },
    create_big_bullet_function(){
        this.bigbulletPool = game.add.group();
        this.bigbulletPool.enableBody = true;
        this.bigbulletPool.createMultiple(10, 'big_bullet');
        this.bigbulletPool.setAll('anchor.x', 0.5);
        this.bigbulletPool.setAll('anchor.y', 0.5);  

        this.bigbulletPoolL = game.add.group();
        this.bigbulletPoolL.enableBody = true;
        this.bigbulletPoolL.createMultiple(10, 'big_bullet_left');
        this.bigbulletPoolL.setAll('anchor.x', 0.5);
        this.bigbulletPoolL.setAll('anchor.y', 0.5);  

        this.bigbulletPoolR = game.add.group();
        this.bigbulletPoolR.enableBody = true;
        this.bigbulletPoolR.createMultiple(10, 'big_bullet_right');
        this.bigbulletPoolR.setAll('anchor.x', 0.5);
        this.bigbulletPoolR.setAll('anchor.y', 0.5);  

        this.bignextShotAt = 0;
        this.bigshotDelay = 100;   

        this.bigbulletTime = 0;
    },
    create_bullet2_function(){
        this.bulletPool2 = game.add.group();
        this.bulletPool2.enableBody = true;
        this.bulletPool2.createMultiple(500, 'bullet');
        this.bulletPool2.setAll('anchor.x', 0.5);
        this.bulletPool2.setAll('anchor.y', 0.5);  
        this.nextShotAt = 0;
        this.shotDelay = 100;   

        this.bulletTime2 = 0;
    },
    create_big_bullet2_function(){
        this.bigbulletPool2 = game.add.group();
        this.bigbulletPool2.enableBody = true;
        this.bigbulletPool2.createMultiple(10, 'big_bullet');
        this.bigbulletPool2.setAll('anchor.x', 0.5);
        this.bigbulletPool2.setAll('anchor.y', 0.5);  

        this.bigbulletPoolL2 = game.add.group();
        this.bigbulletPoolL2.enableBody = true;
        this.bigbulletPoolL2.createMultiple(10, 'big_bullet_left');
        this.bigbulletPoolL2.setAll('anchor.x', 0.5);
        this.bigbulletPoolL2.setAll('anchor.y', 0.5);  

        this.bigbulletPoolR2 = game.add.group();
        this.bigbulletPoolR2.enableBody = true;
        this.bigbulletPoolR2.createMultiple(10, 'big_bullet_right');
        this.bigbulletPoolR2.setAll('anchor.x', 0.5);
        this.bigbulletPoolR2.setAll('anchor.y', 0.5);  


        this.bignextShotAt = 0;
        this.bigshotDelay = 100;   

        this.bigbulletTime2 = 0;
    },
    create_boss_bullet_function(){
        this.bossBullets = game.add.group();
        this.bossBullets.enableBody = true;
        this.bossBullets.createMultiple(100, 'destroyer');
        this.bossBullets.setAll('anchor.x', 0.5);
        this.bossBullets.setAll('anchor.y', 0.5);

        this.bossBulletsl = game.add.group();
        this.bossBulletsl.enableBody = true;
        this.bossBulletsl.createMultiple(100, 'destroyer');
        this.bossBulletsl.setAll('anchor.x', 0.5);
        this.bossBulletsl.setAll('anchor.y', 0.5);

        this.bossBulletsr = game.add.group();
        this.bossBulletsr.enableBody = true;
        this.bossBulletsr.createMultiple(100, 'destroyer');
        this.bossBulletsr.setAll('anchor.x', 0.5);
        this.bossBulletsr.setAll('anchor.y', 0.5);
    },
    create_shot_function(){
        this.cursors = game.input.keyboard.createCursorKeys();
        this.instructions = game.add.text(400, 500, 'Level 2\n', {font: '60px monospace', fill: '#fff', align: 'center'});
        this.instructions.anchor.setTo(0.5);
        this.instExpire = game.time.now + 5000;
    },

    enemyHit: function(bullet, enemy) {
        bullet.kill();
        enemy.kill();

        this.score+=100;
        this.scoreText.text = this.scoreString + this.score;
        var explosion = game.add.sprite(enemy.x, enemy.y, 'explosion');
        explosion.anchor.setTo(0.5);
        explosion.animations.add('boom');
        explosion.play('boom', 15, false, true);
        //audio
        this.sound.play('explosion_sound');

      },
    bossHit: function(boss,bullet){
        console.log(this.bossblood);
        bullet.kill();
        if(this.bossblood==0){
            boss.kill();
            this.bossBullets.kill();
            this.bossBulletsl.kill();
            this.bossBulletsr.kill();
        }
        else{
            this.bossblood--;
            boss.animations.add('boo',[0, 1, 2,3,0], 20, false);
            boss.play('boo');
        }
        var explosion = game.add.sprite(boss.x, boss.y, 'explosion');
        explosion.anchor.setTo(0.5);
        explosion.animations.add('boom');
        explosion.play('boom', 15, false, true);
        //audio
        this.sound.play('explosion_sound');
      },

    playerHit: function(player, enemy) { 
        enemy.kill();

        this.emitter = this.game.add.emitter(player.x, player.y);
        this.emitter.makeParticles('particle', 1, 500, false, false);
        this.emitter.explode(10000, 50);

        var explosion = game.add.sprite(player.x, player.y, 'explosion');
        explosion.anchor.setTo(0.5);
        explosion.animations.add('boom');
        explosion.play('boom', 15, false, true);

        if(this.blood!=0){
            this.blood--;
            this.bloodText.text = this.bloodString + this.blood;
        }
        else {
            player.kill();
            this.lose();
        }
    },
    playerHit2: function(player, enemy) { 
        enemy.kill();

        this.emitter = this.game.add.emitter(player.x, player.y);
        this.emitter.makeParticles('particle', 1, 500, false, false);
        this.emitter.explode(10000, 50);
        
        var explosion = game.add.sprite(player.x, player.y, 'explosion');
        explosion.anchor.setTo(0.5);
        explosion.animations.add('boom');
        explosion.play('boom', 15, false, true);

        if(this.blood2!=0){
            this.blood2--;
            this.bloodText2.text = this.bloodString2 + this.blood2;
        }
        else {
            player.kill();
            this.lose();
        }
    },
    enemy_fire: function(){
        var ebullet = this.enemyBullets.getFirstExists(false);
        this.enemyPool.forEachExists(function(enemy) {
            if(ebullet) {
                if(game.time.now > (enemy.bulletTime || 0)) {
                    ebullet.reset(enemy.x, enemy.y );
                    ebullet.body.velocity.y = 400;
                    //control the frequency 
                    enemy.bulletTime = game.time.now + 700;
                }
            }
        },this);
    },
    boss_fire: function(){
        var bossb = this.bossBullets.getFirstExists(false);
        if(bossb){
            if(game.time.now > (this.boss.bulletTime || 0)) {
                bossb.reset(this.boss.x, this.boss.y-20);
                bossb.body.velocity.y = 400;
                this.boss.bulletTime = game.time.now + 700;
            }
        }
        
    },
    boss_firel: function(){
        var bossbl = this.bossBulletsl.getFirstExists(false);
        if(bossbl){
            if(game.time.now > (this.boss.bulletTimel || 0)) {
                bossbl.reset(this.boss.x+20, this.boss.y-20);
                bossbl.body.velocity.y = 400;
                this.boss.bulletTimel = game.time.now + 700;
            }
        }
        
    },
    boss_firer: function(){
        var bossbr = this.bossBulletsr.getFirstExists(false);
        if(bossbr){
            if(game.time.now > (this.boss.bulletTimer || 0)) {
                bossbr.reset(this.boss.x-20, this.boss.y-20);
                bossbr.body.velocity.y = 400;
                this.boss.bulletTimer = game.time.now + 700;
            }
        }
        
    },
    fire: function() { 
        //audio
        this.player_fire.play();
        this.player_fire.loop = false;

        if (!this.player.alive || this.nextShotAt>game.time.now) {
          return;
        }
        if (this.bulletPool.countDead()==0) {
          return;
        }
        this.nextShotAt = game.time.now + this.shotDelay;
        
        var bullet = this.bulletPool.getFirstExists(false);
        bullet.reset(this.player.x, this.player.y-20);
        bullet.body.velocity.y = -500;

    },
    fire2: function() { 
        //audio
        this.player_fire.play();
        this.player_fire.loop = false;

        if (!this.player2.alive || this.nextShotAt2>game.time.now) {
          return;
        }
        if (this.bulletPool2.countDead()==0) {
          return;
        }
        this.nextShotAt2 = game.time.now + this.shotDelay;
        
        var bullet2 = this.bulletPool2.getFirstExists(false);
        bullet2.reset(this.player2.x, this.player2.y-20);
        bullet2.body.velocity.y = -500;

    },
    bigfire: function() { 
        //audio
        this.player_big_fire.play();
        this.player_big_fire.loop = false;

        if (!this.player.alive || this.bignextShotAt>game.time.now) {
          return;
        }
        if (this.bigbulletPool.countDead()==0) {
            return;
          }
          if (this.bigbulletPoolL.countDead()==0) {
              return;
            }
            if (this.bigbulletPoolR.countDead()==0) {
              return;
            }
        this.bignextShotAt = game.time.now + this.bigshotDelay;
        
        var bigbullet = this.bigbulletPool.getFirstExists(false);
        bigbullet.reset(this.player.x, this.player.y-20);
        bigbullet.body.velocity.y = -500;

        var bigbulletL = this.bigbulletPoolL.getFirstExists(false);
        bigbulletL.reset(this.player.x, this.player.y-20);
        bigbulletL.body.velocity.y = -200;
        bigbulletL.body.velocity.x = -200;

        var bigbulletR = this.bigbulletPoolR.getFirstExists(false);
        bigbulletR.reset(this.player.x, this.player.y-20);
        bigbulletR.body.velocity.y = -200;
        bigbulletR.body.velocity.x = 200;
    },
    bigfire2: function() { 
        //audio
        this.player_big_fire.play();
        this.player_big_fire.loop = false;

        if (!this.player2.alive || this.bignextShotAt2>game.time.now) {
          return;
        }
        if (this.bigbulletPool2.countDead()==0) {
          return;
        }
        if (this.bigbulletPoolL2.countDead()==0) {
            return;
          }
          if (this.bigbulletPoolR2.countDead()==0) {
            return;
          }
        this.bignextShotAt2 = game.time.now + this.bigshotDelay;
        
        var bigbullet2 = this.bigbulletPool2.getFirstExists(false);
        bigbullet2.reset(this.player2.x, this.player2.y-20);
        bigbullet2.body.velocity.y = -500;

        var bigbulletL2 = this.bigbulletPoolL2.getFirstExists(false);
        bigbulletL2.reset(this.player2.x, this.player2.y-20);
        bigbulletL2.body.velocity.y = -200;
        bigbulletL2.body.velocity.x = -200;

        var bigbulletR2 = this.bigbulletPoolR2.getFirstExists(false);
        bigbulletR2.reset(this.player2.x, this.player2.y-20);
        bigbulletR2.body.velocity.y = -200;
        bigbulletR2.body.velocity.x = 200;
    },
    levelup: function(){
        this.level++;
        this.score+=100;
        this.blood ++;
        this.blood2 ++;
        this.bloodText.text = this.bloodString + this.blood;
        this.bloodText2.text = this.bloodString2 + this.blood2;
        this.create_big_bullet_function();
        this.create_big_bullet2_function();
        this.levelText = game.add.text(300, 300,"level up", { font: '34px Arial', fill: '#fff' });
        game.time.events.add(4000, function(){this.levelText.kill();},this);
        return;
    },
    lose: function(){
       this.bg_music.pause();
        game.state.start('lose');
    },


    
};