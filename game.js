var game;
game = new Phaser.Game(800,600,Phaser.AUTO,'canvas');

game.state.add('boot', bootState);
game.state.add('load', loadState);
game.state.add('menu', menuState);
game.state.add('play', playState);
game.state.add('win', winState);
game.state.add('lose', loseState);
game.state.add('second', secondState);
game.state.start('boot');