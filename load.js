var loadState = {
    ready:false,
    preload: function(){
        var nameLabel = game.add.text(80,80,'Loading...',{font: '50px Arial', fill:'#FFFFFF'});
        game.load.image('sea', 'assets/sea.png');
        game.load.image('green', 'assets/green.jpg');
      game.load.image('bullet', 'assets/bullet.png');
      game.load.image('big_bullet','assets/center.png');
      game.load.image('big_bullet_left','assets/left.png');
      game.load.image('big_bullet_right','assets/right.png');
      game.load.image('enemy_bullet','assets/enemy-bullet.png');
      game.load.image('background', 'assets/background.jpg');
      game.load.image('destroyer', 'assets/bullet-burst.png');
      game.load.image('player2', 'assets/player2.png');
      game.load.image('particle', 'assets/particle.png');
      game.load.spritesheet('boss','assets/boss.png',93,75);
      game.load.spritesheet('enemy', 'assets/enemy.png', 32, 32);
      game.load.spritesheet('attack', 'assets/attack.png', 40, 600);
      game.load.spritesheet('enemy2', 'assets/shooting-enemy.png', 32, 32);
      game.load.spritesheet('explosion', 'assets/explosion.png', 32, 32);
      game.load.spritesheet('player', 'assets/player.png', 64, 64);    
        game.load.audio('explosion_sound','assets/explosion.ogg');
        game.load.audio('bg_music','assets/bg_music.mp3');
        game.load.audio('enemy_fire_sound','assets/enemy-fire.wav');
        game.load.audio('player_explosion_sound','assets/player-explosion.ogg');
        game.load.audio('player_fire_sound','assets/player-fire.wav');
        game.load.audio('player_big_fire_sound','assets/player-big-fire.mp3');

    },

update: function() {
    //  Make sure all mp3s have been decoded before starting the game
    if (this.ready) {
      return;
    }
    if (game.cache.isSoundDecoded('explosion_sound') &&
        game.cache.isSoundDecoded('bg_music') &&
        game.cache.isSoundDecoded('enemy_fire_sound') &&
        game.cache.isSoundDecoded('player_explosion_sound') &&
        game.cache.isSoundDecoded('player_fire_sound')) {
      this.ready = true;
      game.state.start('menu');  
    } 
  },
};