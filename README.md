# Software Studio 2019 Spring Assignment 2
## Notice
* Replace all [xxxx] to your answer

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Complete game process|15%|Y|
|Basic rules|20%|Y|
|Jucify mechanisms|15%|Y|
|Animations|10%|Y|
|Particle Systems|10%|Y|
|UI|5%|Y|
|Sound effects|5%|Y|
|Leaderboard|5%|N|

## Website Detail Description
總共設計兩關,第一關只要達到一定分數就可以上第二關,此外一定分數會level up,玩家們的血量會增加.之後會出現boss.若其中一玩家血量歸0則結束遊戲
玩家一:wasd控制,z x 攻擊
玩家二:上下左右控制,l k攻擊
# Basic Components Description : 
1. Jucify mechanisms : 
第一關和第二關:
![](https://i.imgur.com/fVySypc.png)
![](https://i.imgur.com/m4w0UKI.jpg)
大絕:
![](https://i.imgur.com/Pz3Qz8S.jpg)

2. Animations : 
利用animation.add 實作
![](https://i.imgur.com/IFcvwDl.png)

4. Particle Systems : 
若炸到玩家則會觸發
![](https://i.imgur.com/Ccfkywl.png)

6. Sound effects : 
有射擊音效和被擊中的音效
8. Leaderboard : 
無

# Bonus Functions Description : 
1. Multi player game :Off line :
如上圖
3. Boss
Unique movement and attack mode : 

![](https://i.imgur.com/PHLE97f.jpg)
boss會有三發子彈,移動方式是用rnd來隨機決定他的移動速度
![](https://i.imgur.com/dRveJhI.png)




