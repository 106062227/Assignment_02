var menuState = {
    create:function(){
        this.background = game.add.tileSprite(0, 0, 800, 600, 'background');
        var nameLabel = game.add.text(250,100,'Raiden',{font: '100px Arial', fill:'#FFFFFF'});
        var startLabel = game.add.text(220,400,'press w to start',{font: '50px Arial', fill:'#FFFFFF'});
        var wkey = game.input.keyboard.addKey(Phaser.Keyboard.W);
        wkey.onDown.addOnce(this.start,this);
    },
    start:function(){
        game.state.start('play');   
    }
};